# Software Heritage Helper

<p align="center">
    <a href="https://archive.softwareheritage.org/browse/origin/?origin_url=https://gitlab.com/xgqt/xgqt-js-app-softwareheritage-helper/">
        <img src="https://archive.softwareheritage.org/badge/origin/https://gitlab.com/xgqt/xgqt-js-app-softwareheritage-helper/">
    </a>
    <a href="https://gitlab.com/xgqt/xgqt-js-app-softwareheritage-helper/pipelines">
        <img src="https://gitlab.com/xgqt/xgqt-js-app-softwareheritage-helper/badges/master/pipeline.svg">
    </a>
</p>

![logo](./logo.png "logo")

Archive code to Software Heritage while browsing the Web.


## Showcase

![showcase](./showcase.jpg "showcase")


## License

Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>

Licensed under the GNU GPL v2 License

SPDX-License-Identifier: GPL-2.0-or-later
