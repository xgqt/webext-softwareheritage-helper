;; -*- no-byte-compile: t -*-

;; Directory Local Variables
;; For more information see (info "(emacs) Directory Variables")

((find-file . ((require-final-newline . t)
               (show-trailing-whitespace . t)))
 (python-mode . ((indent-tabs-mode . nil)
                 (tab-width . 4)))
 (html-mode . ((tab-width . 2)))
 (js-mode . ((tab-width . 2)))
 (makefile-mode . ((indent-tabs-mode . t)))
 (yaml-mode . ((tab-width . 2))))
