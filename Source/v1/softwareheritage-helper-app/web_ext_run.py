#!/usr/bin/env python3


"""

""" """

This file is part of softwareheritage-helper - archive code to Software Heritage.
Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
Licensed under the GNU GPL v2 License
SPDX-License-Identifier: GPL-2.0-or-later

softwareheritage-helper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

softwareheritage-helper is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with softwareheritage-helper.  If not, see <https://www.gnu.org/licenses/>.
"""


from os import chdir
from os import path

from subprocess import run
from sys import argv


def main():
    """! Main."""

    script_path = path.realpath(__file__)
    script_root = path.dirname(script_path)

    leftover_arguments = argv[1::]
    command_arguments = [
        "web-ext",
        "run",
        "--verbose",
        "--source-dir",
        path.join(script_root, "src"),
        "--url",
        "https://gitlab.com/xgqt/xgqt-js-app-softwareheritage-helper/"
    ] + leftover_arguments

    print(f" * Entering directory: {script_root}")
    chdir(script_root)

    command_string = " ".join(command_arguments)
    print(f" * Executing command: {command_string}")

    run(command_arguments, check=True)


if __name__ == "__main__":
    main()
