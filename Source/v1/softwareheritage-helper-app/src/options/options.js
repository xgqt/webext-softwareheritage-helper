// This file is part of softwareheritage-helper - archive code to Software Heritage.
// Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
// Licensed under the GNU GPL v2 License
// SPDX-License-Identifier: GPL-2.0-or-later

// softwareheritage-helper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.

// softwareheritage-helper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with softwareheritage-helper.  If not, see <https://www.gnu.org/licenses/>.


'use strict';


import { defaults } from './defaults.js';


async function saveOptions(event) {
  event.preventDefault();

  chrome.storage.sync.set({
    instanceUrl: document.querySelector('#instance_url').value,
    savesNumber: document.querySelector('#saves_number').value
  });
}

async function defaultOptions() {
  document.querySelector('#instance_url').value = defaults.instanceUrl;
  document.querySelector('#saves_number').value = defaults.savesNumber;
}

async function restoreOptions() {
  chrome.storage.sync.get(
    ['instanceUrl', 'savesNumber'],
    (result) => {
      document
        .querySelector('#instance_url')
        .value = result.instanceUrl || defaults.instanceUrl;

      document
        .querySelector('#saves_number')
        .value = result.savesNumber || defaults.savesNumber;
    });
}


(async () => {
  document.addEventListener('DOMContentLoaded', restoreOptions);

  document.querySelector('form')
    .addEventListener('submit', saveOptions);

  document.querySelector('#defaults')
    .addEventListener('click', defaultOptions);
})();
