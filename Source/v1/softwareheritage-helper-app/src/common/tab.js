// This file is part of softwareheritage-helper - archive code to Software Heritage.
// Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
// Licensed under the GNU GPL v2 License
// SPDX-License-Identifier: GPL-2.0-or-later

// softwareheritage-helper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.

// softwareheritage-helper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with softwareheritage-helper.  If not, see <https://www.gnu.org/licenses/>.


'use strict';


export { getTabUrl, getUrlRepo, getTabRepo };


const supportedSiteRegexp = new RegExp(
  '^https://git(hub|lab).com/[^/]+/[^/]+'
);


function getTabUrl(tabs) {
  return tabs[0].url;
}

function getUrlRepo(url) {
  const match = (
    (typeof url === 'string') ? url.match(supportedSiteRegexp) : false
  );
  return (Array.isArray(match)) ? match[0] : false;
}

function getTabRepo(tabs) {
  return getUrlRepo(getTabUrl(tabs));
}
