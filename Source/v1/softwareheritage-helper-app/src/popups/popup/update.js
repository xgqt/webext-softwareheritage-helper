// This file is part of softwareheritage-helper - archive code to Software Heritage.
// Copyright (c) 2022, Maciej Barć <xgqt@riseup.net>
// Licensed under the GNU GPL v2 License
// SPDX-License-Identifier: GPL-2.0-or-later

// softwareheritage-helper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.

// softwareheritage-helper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with softwareheritage-helper.  If not, see <https://www.gnu.org/licenses/>.


'use strict';


import { defaults } from '../../options/defaults.js';

import { getTabUrl, getUrlRepo } from '../../common/tab.js';
import { joinSoftwareheritageBrowseUrl } from '../../common/url.js';
import { sendRepoSaveRequest } from '../../common/send.js';
import { submitRepoArchival } from '../../common/submit.js';

export { updateCurrentRepoFromTabs };


async function getRepoSaves(instanceUrl, savesNumber, repoUrl) {
  let request = sendRepoSaveRequest(instanceUrl, repoUrl, 'GET');

  function onComplete(_event) {
    const responseJson = JSON.parse(request.responseText);

    if (!responseJson.exception) {
      const succeededSaves = responseJson
        .filter((obj) => obj.save_task_status === 'succeeded')
        .slice(0, Number(savesNumber));
      const recentSaves = succeededSaves
        .map((obj) =>
          `<li>${(new Date(obj.visit_date)).toLocaleString()}</li>`)
        .join('');

      document
        .getElementById('repo_saves')
        .innerHTML = `
<section>
  <h2>Recent saves:</h2>
  <ul>
    ${recentSaves}
  </ul>
</section>
`;
    }
  }

  request.addEventListener('loadend', onComplete);
}

async function updateCurrentRepoFromTabs(tabs) {
  const tabUrl = getTabUrl(tabs);
  const tabRepo = getUrlRepo(tabUrl);

  if (tabRepo) {
    chrome.storage.sync.get(
      ['instanceUrl', 'savesNumber'],
      (result) => {
        const instanceUrl = result.instanceUrl || defaults.instanceUrl;
        const savesNumber = result.savesNumber || defaults.savesNumber;

        document
          .getElementById('current_repo')
          .innerHTML = `
<div>
  <p>
    Repository: <a href="${tabRepo}">${tabRepo}</a>
  </p>
  <p>
    <img id="view_on_softwareheritage"
         src="${instanceUrl}/badge/origin/${tabRepo}"
         title='The "${tabRepo}" archive badge'>
  </p>
  <p>
    <button id="submit_for_archival"
            class="button"
            title='Submit "${tabRepo}" for archival'>
      Submit for archival
    </button>
  </p>
  <div id="repo_saves">
  </div>
</div>
`;
        getRepoSaves(instanceUrl, savesNumber, tabRepo);

        document
          .getElementById('view_on_softwareheritage')
          .addEventListener(
            'click',
            (async () =>
              window.open(
                joinSoftwareheritageBrowseUrl(instanceUrl, tabUrl),
                '_blank'
              ).focus())
          );

        document
          .getElementById('submit_for_archival')
          .addEventListener(
            'click',
            (async () => submitRepoArchival(instanceUrl, tabRepo))
          );
      });
  } else {
    document
      .getElementById('current_repo')
      .innerHTML = `
<div>
  <h3 class="warning">Not on a repository page!</h3>
  ${(tabUrl ? `<p>URL "${tabUrl}" is not supported.</p>` : '')}
</div>
`;
  }
}
